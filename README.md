# Deploy

Run `zappa deploy dev`. Your `~/.aws/credentials` file needs to have a profile called `acyclic` with the credentials.
