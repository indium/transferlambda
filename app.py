from flask import Flask
from flask import request
from web3 import Web3
from web3.middleware import geth_poa_middleware
import json
from hexbytes import HexBytes

class HexJsonEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, HexBytes):
            return obj.hex()
        return super().default(obj)

app = Flask(__name__)

@app.route('/', methods=['GET'])
def transfer_get():
    return 'Send POST request'

@app.route('/', methods=['POST'])
def transfer_post():
    rpc_node_url = request.args.get('rpc')
    receiver = request.args.get('receiver')
    sender = request.args.get('sender')
    privkey = request.args.get('privkey')
    amount = int(request.args.get('amount'))
    gaslimit = int(request.args.get('gaslimit'))
    gasprice = int(request.args.get('gasprice'))
    chain = int(request.args.get('chain'))
    data = request.args.get('data')

    w3 = Web3(Web3.HTTPProvider(rpc_node_url))
    w3.middleware_stack.inject(geth_poa_middleware, layer=0)
    w3.eth.getBalance(receiver)
    w3.eth.getBalance(sender)

    transaction = {
            'to': receiver,
            'value': amount,
            'gas': gaslimit,
            'gasPrice': gasprice,
            'nonce': w3.eth.getTransactionCount(sender),
            'chainId': chain,
            'data': str.encode(data)
    }

    signed = w3.eth.account.signTransaction(transaction, privkey)
    txn_hash = w3.eth.sendRawTransaction(signed.rawTransaction)
    receipt = w3.eth.getTransactionReceipt(txn_hash)
    return json.dumps(dict(receipt), cls=HexJsonEncoder)